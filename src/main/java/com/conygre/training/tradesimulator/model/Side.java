package com.conygre.training.tradesimulator.model;

//Enumeration to determine of a trade is a buy or sell request
public enum Side {
    BUY("BUY"),
    SELL("SELL");

    private String side;

    public String getSide() {
        return side;
    }

    public void setSide(String side) {
        this.side = side;
    }

    private Side(String side) {
        this.side = side;
    }
}
