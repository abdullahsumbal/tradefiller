package com.conygre.training.tradesimulator.model;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

//A holdings object within our MongoDB holdings table
public class Holding {
    @Id
    private ObjectId id;
    private String ticker;
    private int quantity;

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getTicker() {
        return ticker;
    }

    public void setTicker(String ticker) {
        this.ticker = ticker;
    }
}

