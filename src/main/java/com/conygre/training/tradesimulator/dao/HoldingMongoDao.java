package com.conygre.training.tradesimulator.dao;

import com.conygre.training.tradesimulator.model.Holding;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface HoldingMongoDao extends MongoRepository<Holding, ObjectId> {

	public Holding findByTicker(String ticker);
}