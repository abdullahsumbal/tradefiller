package com.conygre.training.tradesimulator.sim;

import java.util.List;

import com.conygre.training.tradesimulator.dao.HoldingMongoDao;
import com.conygre.training.tradesimulator.dao.TradeMongoDao;
import com.conygre.training.tradesimulator.model.Holding;
import com.conygre.training.tradesimulator.model.Side;
import com.conygre.training.tradesimulator.model.Trade;
import com.conygre.training.tradesimulator.model.TradeState;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class TradeSim {
    private static final Logger LOG = LoggerFactory.getLogger(TradeSim.class);

    @Autowired
    private TradeMongoDao tradeDao;

    
    @Autowired
    private HoldingMongoDao holdingDao;

    @Transactional
    public List<Trade> findTradesForProcessing(){
        List<Trade> foundTrades = tradeDao.findByState(TradeState.CREATED);

        for(Trade thisTrade: foundTrades) {
            thisTrade.setState(TradeState.PROCESSING);
            tradeDao.save(thisTrade);
        }

        return foundTrades;
    }

    @Transactional
    public List<Trade> findTradesForFilling(){
        List<Trade> foundTrades = tradeDao.findByState(TradeState.PROCESSING);

        for(Trade thisTrade: foundTrades) {
            if((int) (Math.random()*10) > 10) {
                thisTrade.setState(TradeState.REJECTED);
            }
            else {
                Side side = thisTrade.getSide();
                if(side.equals(Side.BUY)){
                    buyHolding(thisTrade);
                }else{
                    sellHolding(thisTrade);
                }                
            }
            tradeDao.save(thisTrade);
        }

        return foundTrades;
    }

    @Scheduled(fixedRateString = "${scheduleRateMs:3000}")
    public void runSim() {
        LOG.info("Main loop running!");

        int tradesForFilling = findTradesForFilling().size();
        LOG.info("Found " + tradesForFilling + " trades to be filled/rejected");

        int tradesForProcessing = findTradesForProcessing().size();
        LOG.info("Found " + tradesForProcessing + " trades to be processed");

    }

    private void buyHolding(Trade trade){
        String ticker = trade.getTicker();
        Holding holding = holdingDao.findByTicker(ticker);
        int quantity = trade.getQuantity(); 
        if(holding == null){
            holding = new Holding();
            holding.setQuantity(quantity);
            holding.setTicker(ticker);
            holdingDao.insert(holding);
        }else{
            int quantityInHolding = holding.getQuantity();
            holding.setQuantity(quantity + quantityInHolding);
        }
        holdingDao.save(holding);
        trade.setState(TradeState.FILLED);
    }

    private void sellHolding(Trade trade){
        String ticker = trade.getTicker();
        Holding holding = holdingDao.findByTicker(ticker);
        int quantity = trade.getQuantity();

        if(holding == null){
            trade.setState(TradeState.REJECTED);
            return;
        }else{
            int quantityInHolding = holding.getQuantity();
            if(quantityInHolding >= quantity){
                holding.setQuantity(quantityInHolding - quantity);
                holdingDao.save(holding);
                trade.setState(TradeState.FILLED);
            }else{
                trade.setState(TradeState.REJECTED);
            }
        }
    }
}
